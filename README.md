<div align="center">
    <img src="https://i.imgur.com/BQgvynG.png" height="200" width="200">
    <h1>ChatMoji</h1>
    <strong>Plugin for creating your custom chat emotes!</strong><br><br>
    <img src="https://forthebadge.com/images/badges/built-with-love.svg" height="30">&nbsp;
    <a href="https://discord.gg/eEpgCme"><img src="https://img.shields.io/discord/424522113953562636.svg?style=for-the-badge" height="30"></a>&nbsp;
</div>

---

**ChatMoji** is a plugin for creating your custom chat emoticons! You can add your custom emote in ``emotes.yml`` file. Also ChatMoji supports permissions for every emote!

## Download ChatMoji
Because we are still working on ChatMoji, download isn't currently available.

## Statistics from bStats
<img src="https://bstats.org/signatures/bukkit/ChatMoji.svg"/>

## Main Contributors
* [Speedy11CZ#0001](https://gitlab.com/Speedy11CZ)
