package mc.reunion.chatmoji;

import mc.reunion.chatmoji.commands.EmotesCommand;
import mc.reunion.chatmoji.listeners.ChatListener;
import mc.reunion.chatmoji.managers.EmoteManager;
import mc.reunion.chatmoji.utils.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class Main extends JavaPlugin {

    private static Main instance;
    private EmoteManager emoteManager = new EmoteManager();
    //private EmotesGUI emotesGUI = new EmotesGUI();

    public static Main getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        getConfig().options().copyDefaults(true);
        saveDefaultConfig();
        if(!new File(getDataFolder(), "emotes.yml").exists()) {
            saveResource("emotes.yml", false);
        }

        emoteManager.loadEmotesFromConfig();

        Metrics metrics = new Metrics(this);

        //Bukkit.getPluginManager().registerEvents(emotesGUI, this);
        Bukkit.getPluginManager().registerEvents(new ChatListener(), this);

        getCommand("emotes").setExecutor(new EmotesCommand());
    }

    @Override
    public void onDisable() {

    }

    public EmoteManager getEmoteManager() {
        return emoteManager;
    }

    //public EmotesGUI getEmotesGUI() {
    //return emotesGUI;
    //}

}
