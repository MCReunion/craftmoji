package mc.reunion.chatmoji.objects;

public class Emote {

    private String name;
    private String placeholder;
    private String replacement;

    public Emote(String name, String placeholder, String replacement) {
        this.name = name;
        this.placeholder = placeholder;
        this.replacement = replacement;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public String getReplacement() {
        return replacement;
    }

    public String getPermission() {
        return "chatmoji.emote." + name.toLowerCase().replaceAll(" ", "_").replaceAll("\\.", "_");
    }
}
