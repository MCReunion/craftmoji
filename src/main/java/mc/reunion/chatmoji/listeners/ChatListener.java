package mc.reunion.chatmoji.listeners;

import mc.reunion.chatmoji.Main;
import mc.reunion.chatmoji.objects.Emote;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.List;
import java.util.stream.Collectors;

public class ChatListener implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        StringBuilder newMessage = new StringBuilder();
        for(String arg : event.getMessage().split(" ")) {
            List<Emote> emotes = Main.getInstance().getEmoteManager().getEmotesAsList().stream().filter(e -> e.getPlaceholder().equals(arg)).collect(Collectors.toList());
            if(emotes.size() > 0) {
                Emote emote = emotes.get(0);
                if(!Main.getInstance().getConfig().getBoolean("use-permissions") || event.getPlayer().hasPermission(emote.getPermission())) {
                    newMessage.append(arg.replace(emote.getPlaceholder(), emote.getReplacement()));
                } else {
                    newMessage.append(arg);
                }
            } else {
                newMessage.append(arg);
            }
            newMessage.append(" ");
        }
        event.setMessage(newMessage.substring(0, newMessage.length() - 1));
    }
}
