package mc.reunion.chatmoji.commands;

import mc.reunion.chatmoji.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class EmotesCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(args.length > 0) {
            if(args[0].equals("reload")) {
                if(!sender.hasPermission("chatmoji.admin")) {
                    sender.sendMessage("§cYou don't have permissions to do this!");
                    return false;
                }
                Main.getInstance().getEmoteManager().reloadEmotes();
                Main.getInstance().reloadConfig();
                sender.sendMessage("§aEmotes and config reloaded successfully!");
            } else {
                sender.sendMessage("§cUnknown argument.");
            }
            return false;
        }

        /*
        if(!(sender instanceof Player)) {
            sender.sendMessage("§cYou can't use this command here!");
            return false;
        }
        if(!sender.hasPermission("chatmoji.command.emotes")) {
            sender.sendMessage("§cYou don't have permissions to do this!");
            return false;
        }
        sender.sendMessage("§aOpening emotes list...");
        Main.getInstance().getEmotesGUI().openGui((Player) sender);
        */
        return false;
    }
}
