package mc.reunion.chatmoji.managers;

import mc.reunion.chatmoji.Main;
import mc.reunion.chatmoji.objects.Emote;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EmoteManager {

    private HashMap<String, Emote> emotes = new HashMap<>();

    public void loadEmotesFromConfig() {
        YamlConfiguration emotes = YamlConfiguration.loadConfiguration(new File(Main.getInstance().getDataFolder(), "emotes.yml"));
        for(String key : emotes.getKeys(false)) {
            ConfigurationSection section = emotes.getConfigurationSection(key);
            this.emotes.put(key.toLowerCase(), new Emote(section.getString("name"), section.getString("placeholder"), section.getString("replacement")));
        }
    }

    public void reloadEmotes() {
        emotes.clear();
        loadEmotesFromConfig();
    }

    public List<Emote> getEmotesAsList() {
        return new ArrayList<>(emotes.values());
    }

    public HashMap<String, Emote> getEmotes() {
        return emotes;
    }
}
